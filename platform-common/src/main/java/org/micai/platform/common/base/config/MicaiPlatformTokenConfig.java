package org.micai.platform.common.base.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Author liuCong
 * @Date 2022/12/13 上午 9:38
 * @ClassName MicaiPlatformAuthConfig
 * @Description
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "micai-platform-auth.token")
public class MicaiPlatformTokenConfig {

    /** JWT */
    private String signKey;
    /** token的长久有效期(单位:分钟) 默认1分钟*/
    private Integer timeout = 1;
    /** token名称 (同时也是cookie名称) */
    private String tokenName;
    /** token前缀*/
    private String tokenPrefix;


    public Integer getTimeout() {
        return timeout * 60;
    }
}
