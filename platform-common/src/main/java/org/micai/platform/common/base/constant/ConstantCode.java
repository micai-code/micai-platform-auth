package org.micai.platform.common.base.constant;

/**
 * @Author liuCong
 * @Date 2021/12/4 15:40
 * @ClassName ConstantCode
 * @Description
 */
public class ConstantCode {

    /**
     * 00
     */
    public static final String STR_DOUBLE_ZONE = "00";

    /**
     * 01
     */
    public static final String STR_Z_ONE = "01";


    /**
     * 2
     */
    public static final String STR_TWO = "2";

    /**
     * 0
     */
    public static final int INT_ZERO = 0;

    /**
     * 1
     */
    public static final int INT_ONE = 1;

    /**
     * 2
     */
    public static final int INT_TWO = 2;

    /**
     * 3
     */
    public static final int INT_THREE = 3;

    /**
     * 7
     */
    public static final int INT_SEVEN = 7;

    /**
     * 10
     */
    public static final int INT_TEN = 10;

    /**
     * 100
     */
    public static final int INT_ONE_HUNDRED = 100;

    /**
     * 200
     */
    public static final int INT_TWO_HUNDRED = 200;

    /**
     * 角色前缀
     */
    public static final String DEFAULT_ROLE_PREFIX = "ROLE_";

    /**
     * 认证
     */
    public static final String AUTHORIZATION = "Authorization";

    /**
     * 认证
     */
    public static final String REFRESH_AUTHORIZATION = "refreshAuthorization";

    /**
     * 通行前缀
     */
    public static final String HEADER_BEARER = "Bearer ";

    /**
     * 刷新token
     */
    public static final String REFRESH_TOKEN = "refreshToken";

    /**
     * content json 类型
     */
    public static final String CONTENT_TYPE_JSON = "application/json;charset=UTF-8";

    /**
     * 1024   顶
     */
    public static final int DING = 1024;

    /**
     * SIZE
     */
    public static final String SIZE = "size";

    /**
     * WIDTH
     */
    public static final String WIDTH = "width";

    /**
     * HEIGHT
     */
    public static final String HEIGHT = "height";


    /**
     * SESSION_KEY_IMAGE_CODE
     */
    public static final String SESSION_KEY = "SESSION_KEY_IMAGE_CODE";



    /**
     * 文件存储-缩略图目录
     */
    public static final String thumbnailPath = "/storage/thumbnails";

    /**
     * 文件存储-头像目录
     */
    public static final String avatarPath = "/storage/avatars";

    /**
     * 默认头像
     */
    public static final String AVATAR = "https://en.gravatar.com/userimage/154673030/b9a54b5b990a61cc074668b2e2a0b8c0.png";

    /**
     * 分隔符
     */
    public static final String SEPARATOR = ",";

    public static final String SEPARATOR_X = "x";

    public static final String ROLE_ADMIN = "admin";

    public static final int PAGE_DEFAULT_SIZE = 10;

    public static final int IDENTITY_STEP = 1; // 自增步进

    public static final int DECREASE_STEP = -1; // 递减

    public static final int TIME_MIN = 1000; // 最小时间单位, 1秒

    // 忽略值
    public static final int IGNORE = -1;

    public static final int ZERO = 0;

    // 禁用状态
    public static final int STATUS_CLOSED = 1;

    /* 状态-初始 */
    public static final int STATUS_NORMAL = 0;

    /* 状态-推荐 */
    public static final int STATUS_FEATURED = 1;

    /* 状态-锁定 */
    public static final int STATUS_LOCKED = 1;

    public static final int STATUS_HIDDEN = 1;

    /**
     * 排序
     */
    public interface order {
        String FEATURED = "featured";
        String NEWEST = "newest";
        String HOTTEST = "hottest";
        String FAVOR = "favors";
    }

    public static final int CODE_BIND = 1;   // bind email
    public static final int CODE_FORGOT = 2; // forgot password
    public static final int CODE_REGISTER = 3;

    public static final int CODE_STATUS_INIT = 0;      // 验证码-初始
    public static final int CODE_STATUS_CERTIFIED = 1; // 验证码-已使用

    public static final int FEATURED_DEFAULT = 0; // 推荐状态-默认
    public static final int FEATURED_ACTIVE = 1;  // 推荐状态-推荐

    /**
     * 未读
     */
    public static final int UNREAD = 0;

    /**
     * 已读
     */
    public static final int READED = 1;

    public static final int MESSAGE_EVENT_FAVOR_POST = 1; // 有人喜欢了你的文章

    public static final int MESSAGE_EVENT_COMMENT = 3; // 有人评论了你

    public static final int MESSAGE_EVENT_COMMENT_REPLY = 4; // 有人回复了你

    public static final String CACHE_USER = "userCaches";
    public static final String CACHE_POST = "postCaches";

    /**
     * 第三方回调配置
     */
    public static final String QQ_CALLBACK = "qq_callback"; // 第三方登录-QQ回调地址
    public static final String QQ_APP_ID = "qq_app_id";            // QQ互联APP_ID
    public static final String QQ_APP_KEY = "qq_app_key";        // QQ互联APP_KEY

    public static final String WEIBO_CALLBACK = "weibo_callback"; // 第三方登录-微博回调地址
    public static final String WEIBO_CLIENT_ID = "weibo_client_id";        // 微博应用CLIENT_ID
    public static final String WEIBO_CLIENT_SERCRET = "weibo_client_sercret";    // 微博应用CLIENT_SERCRET

    public static final String DOUBAN_CALLBACK = "douban_callback";    // 第三方登录-豆瓣回调地址
    public static final String DOUBAN_API_KEY = "douban_api_key";        // 豆瓣API_KEY
    public static final String DOUBAN_SECRET_KEY = "douban_secret_key";        // 豆瓣SECRET_KEY

    public static final String GITHUB_CALLBACK = "github_callback";    // 第三方登录-github回调地址
    public static final String GITHUB_CLIENT_ID = "github_client_id";//github应用CLIENT_ID
    public static final String GITHUB_SECRET_KEY = "github_secret_key";//github应用SECRET_KEY

    public static final String EMAIL_TEMPLATE_CODE = "email_code.ftl";

    public static final String EDITOR_MARKDOWN = "markdown";

    public static final String STORAGE_LIMIT_SIZE = "storage_limit_size";
    public static final String STORAGE_MAX_WIDTH = "storage_max_width";

    public static final String THUMBNAIL_POST_SIZE = "thumbnail_post_size";


    /**
     * 图片code
     */
    public static final String IMAGE_CODE = "imageCode";

    /**
     * BASE_REALM
     */
    public static final String BASE_REALM = "Basic realm=\"MyApp\"";

    /**
     * username
     */
    public static final String USERNAME = "username";

    /**
     * password
     */
    public static final String PASSWORD = "password";

    /**
     * JSESSIONID
     */
    public static final String JSESSIONID = "JSESSIONID";
}
