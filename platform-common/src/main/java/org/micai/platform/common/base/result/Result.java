package org.micai.platform.common.base.result;

import lombok.Data;
import org.micai.platform.common.base.constant.ConstantEnum;

/**
 * 返回数据
 */
@Data
public class Result {
	private Integer code;
	private String message;
	private Object data;
	public Result() {}

	public Result(Integer code, String message) {
		this.code = code;
		this.message = message;
	}

	public Result(Integer code, String message, Object data) {
		this(code, message);
		this.data = data;
	}

	public Result(ConstantEnum constantEnum) {
		this.code = constantEnum.getCode();
		this.message = constantEnum.getMessage();
	}

	public Result(ConstantEnum constantEnum,Object data) {
		this(constantEnum.getCode(), constantEnum.getMessage());
		this.data = data;
	}
}
