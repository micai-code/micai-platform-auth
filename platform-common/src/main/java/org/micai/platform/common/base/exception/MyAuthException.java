package org.micai.platform.common.base.exception;

import org.micai.platform.common.base.constant.ConstantEnum;
import org.springframework.security.core.AuthenticationException;

/**
 * @Author liuCong
 * @Date 2022/1/4 17:18
 * @ClassName SecurityException
 * @Description
 */
public class MyAuthException extends AuthenticationException {

    private Integer errorCode;
    private String errorMessage;

    public MyAuthException(String msg, Throwable cause) {
        super(msg, cause);
        this.errorMessage = msg;
    }

    public MyAuthException(Integer code, String msg) {
        super(msg);
        this.errorCode = code;
        this.errorMessage = msg;
    }

    public MyAuthException(Integer errorCode, String errorMessage, Throwable cause) {
        super("[" + errorCode + "]" + errorMessage, cause);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public MyAuthException(ConstantEnum constantEnum) {
        this(constantEnum.getCode(), constantEnum.getMessage());
    }


    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
