package org.micai.platform.common.base.constant;

import javax.servlet.http.HttpServletResponse;

/**
 * @Author liuCong
 * @Date 2021/12/30 10:15
 * @ClassName ConstantEnum
 * @Description
 */
public enum ConstantEnum {

    SUCCESS(HttpServletResponse.SC_OK, "成功"),

    FAIL(HttpServletResponse.SC_BAD_REQUEST, "失败"),

    TOKEN_ERROR(HttpServletResponse.SC_FORBIDDEN, "无效TOKEN, 请重新登录！"),

    TOKEN_MISS(HttpServletResponse.SC_FORBIDDEN, "token 丢失"),

    USER_CODE_ERROR(HttpServletResponse.SC_UNAUTHORIZED, "用户名或密码错误"),

    NOT_FOUND(HttpServletResponse.SC_NOT_FOUND, "路径不存在，请检查路径是否正确"),

    REQUEST_TIMEOUT(HttpServletResponse.SC_REQUEST_TIMEOUT, "系统调用异常"),

    USER_OLD_PASSWORD(10005,"原密码不正确"),

    USER_FREEZE(10007,"用户已冻结"),

    REDIS_CONNECT_ERROR(10008,"redis 连接异常"),

    PARAM_ERROR(10009, "参数为空"),

    VALIDATION_CODE_ERROR(10010, "验证码失效，请重新获取"),

    USER_EXIST(10011, "该用户已存在"),

    UPDATE_ERROR(10012, "更新失败,请重试"),

    NO_FILE(10013, "未包含文件上传域"),

    TYPE(10014, "不允许的文件格式"),

    SIZE(10015, "文件大小超出限制，最大支持2Mb"),

    EN_TYPE(10016, "请求类型ENTYPE错误"),

    REQUEST(10017, "上传请求异常"),

    IO(10018, "IO异常"),

    DIR(10019, "目录创建失败"),

    UNKNOWN(10020, "未知错误"),

    NO_ROLE(10021, "没有角色信息"),

    NO_ALIYUN_CONFIG(10022, "请先在后台设置阿里云配置信息"),

    NO_PERMISSION(10023, "没有权限信息"),

    SIGNATURE_EX(10024, "签名错误"),

    ILLEGAL_ARG(10025, "非法错误"),

    EXPIRED_JWT(10026, "token过期,请重新登录"),

    DUPLICATE_KEY(10027, "数据库中已存在该记录"),

    RESOURCE_ACCESS(10028, "系统之间调用异常"),

    ACCESS_DENIED(HttpServletResponse.SC_FORBIDDEN, "Token非法参数异常"),

    IMAGE_ERROR(10029, "验证码为空或者验证码输入有误"),

    LOGINED(10030, "账号已被登录"),

    EXPIRED_LOGIN(HttpServletResponse.SC_BAD_REQUEST, "登录超时,请重新登录"),

    DATA_DUPLICATION(10031, "数据重复");





    private Integer code;

    private String message;


    ConstantEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
