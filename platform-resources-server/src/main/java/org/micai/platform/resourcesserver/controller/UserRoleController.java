package org.micai.platform.resourcesserver.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.micai.platform.common.base.constant.ConstantEnum;
import org.micai.platform.common.base.exception.PlatformException;
import org.micai.platform.common.base.result.Result;
import org.micai.platform.resourcesserver.bo.UserRoleDelBo;
import org.micai.platform.resourcesserver.bo.UserRoleSaveBo;
import org.micai.platform.resourcesserver.bo.UserRoleUpdateBo;
import org.micai.platform.resourcesserver.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author liucong
 * @since 2022-01-13
 */
@RestController
@RequestMapping("/userRole")
@Slf4j
@Api(tags = "用户角色管理", value = "用户角色管理")
public class UserRoleController extends BaseController{
    @Autowired
    private UserRoleService userRoleService;


    @ApiOperation(value = "更新用户角色权限", notes = "更新用户角色权限")
    @PostMapping("/update")
    @PreAuthorize("hasAnyAuthority('sys:userRole:update')")
    public Result updateUR(@RequestBody @Valid UserRoleUpdateBo bo){
        Result result;
        try {
            userRoleService.updateUR(bo);
            result = new Result(ConstantEnum.SUCCESS);
        } catch (PlatformException e) {
            log.error("error info:",e);
            result =  new Result(e.getErrorCode(), e.getErrorMessage());
        } catch (Exception e) {
            log.error("error info:",e);
            result =  new Result(ConstantEnum.FAIL);
        }
        return result;
    }

    @ApiOperation(value = "新增用户角色权限", notes = "新增用户角色权限")
    @PostMapping("/save")
    @PreAuthorize("hasAnyAuthority('sys:userRole:save')")
    public Result saveUR(@RequestBody @Valid UserRoleSaveBo bo){
        Result result;
        try {
            userRoleService.saveUR(bo);
            result = new Result(ConstantEnum.SUCCESS);
        } catch (PlatformException e) {
            log.error("error info:",e);
            result =  new Result(e.getErrorCode(), e.getErrorMessage());
        } catch (Exception e) {
            log.error("error info:",e);
            result =  new Result(ConstantEnum.FAIL);
        }
        return result;
    }

    @ApiOperation(value = "删除用户角色权限", notes = "删除用户角色权限")
    @DeleteMapping("/del")
    @PreAuthorize("hasAnyAuthority('sys:userRole:del')")
    public Result delUR(@RequestBody @Valid UserRoleDelBo bo){
        Result result;
        try {
            userRoleService.delUR(bo);
            result = new Result(ConstantEnum.SUCCESS);
        } catch (PlatformException e) {
            log.error("error info:",e);
            result =  new Result(e.getErrorCode(), e.getErrorMessage());
        } catch (Exception e) {
            log.error("error info:",e);
            result =  new Result(ConstantEnum.FAIL);
        }
        return result;
    }
}

