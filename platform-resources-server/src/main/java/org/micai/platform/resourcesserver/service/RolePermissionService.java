package org.micai.platform.resourcesserver.service;

import org.micai.platform.resourcesserver.bo.RolePermissionDelBo;
import org.micai.platform.resourcesserver.bo.RolePermissionSaveBo;
import org.micai.platform.resourcesserver.bo.RolePermissionUpdateBo;
import org.micai.platform.resourcesserver.entity.RolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liucong
 * @since 2021-12-04
 */
public interface RolePermissionService extends IService<RolePermission> {

    void updateRP(RolePermissionUpdateBo bo) throws Exception;

    void saveRP(RolePermissionSaveBo bo) throws Exception;

    void delRP(RolePermissionDelBo bo) throws Exception;
}
