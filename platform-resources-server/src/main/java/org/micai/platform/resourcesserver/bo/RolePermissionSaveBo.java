package org.micai.platform.resourcesserver.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author liuCong
 * @Date 2022/1/13 14:35
 * @ClassName RolePermissionSaveBo
 * @Description
 */
@Data
@ApiModel(value = "RolePermissionSaveBo")
public class RolePermissionSaveBo implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "角色id不能为空")
    @ApiModelProperty(value = "角色id")
    private Long roleId;

    @NotNull(message = "权限id不能为空")
    @ApiModelProperty(value = "权限id")
    private Long permissionId;


}
