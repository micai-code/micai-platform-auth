package org.micai.platform.resourcesserver.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @Author liuCong
 * @Date 2021/12/7 10:32
 * @ClassName UserAuthenticationDto
 * @Description
 */
@ApiModel(value = "获取用户的角色/权限的实体")
@Data
@Accessors(chain = true)
public class UserAuthenticationDto {

    private Long id;
    private List<String> roles;
    private List<String> permissions;
}
