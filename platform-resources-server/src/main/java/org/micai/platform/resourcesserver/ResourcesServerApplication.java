package org.micai.platform.resourcesserver;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author liuCong
 * @Date ${DATE} ${TIME}
 * @ClassName ${NAME}
 * @Description 资源服务的启动类
 */
@SpringBootApplication
@MapperScan("org.micai.platform.resourcesserver.mapper")
public class ResourcesServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ResourcesServerApplication.class,args);
    }
}