package org.micai.platform.resourcesserver.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author liuCong
 * @Date 2022/1/13 17:03
 * @ClassName PermissionMenuSaveBo
 * @Description
 */
@Data
@ApiModel(value = "PermissionMenuSaveBo")
public class PermissionMenuSaveBo implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "权限id不能为空")
    @ApiModelProperty(value = "权限id")
    private Long permissionId;

    @NotNull(message = "菜单id不能为空")
    @ApiModelProperty(value = "菜单id")
    private Long menuId;
}
