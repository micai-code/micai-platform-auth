package org.micai.platform.resourcesserver.config;

import org.micai.platform.common.base.config.MicaiPlatformResourcesConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

/**
 * @Author liuCong
 * @Date 2022/11/24 下午 3:38
 * @ClassName JwtTokenConfig
 * @Description 配置Jwt令牌服务，生成jwt格式的token
 */
@Configuration
public class TokenConfig {


    // jwt模式token
    @Autowired
    private MicaiPlatformResourcesConfig micaiPlatformResourcesConfig;

    @Bean
    public JwtTokenStore jwtTokenStore(){
        return new JwtTokenStore(jwtAccessTokenConverter());
    }


    /**
     * @Author : liuCong
     * @Date : 2022/11/25 下午 4:16
     * @Description :TokenEnhancer的子类，帮助程序在JWT编码的令牌值和OAuth身份验证信息之间进行转换（在两个方向上），同时充当TokenEnhancer授予令牌的时间。
     * @Throws : //
     * @Params : []
     * @Return : org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter
     **/
    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter(){
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setSigningKey(micaiPlatformResourcesConfig.getSignKey());
        return converter;
    }
}
