package org.micai.platform.resourcesserver.service;

import org.micai.platform.resourcesserver.bo.UserRoleDelBo;
import org.micai.platform.resourcesserver.bo.UserRoleSaveBo;
import org.micai.platform.resourcesserver.bo.UserRoleUpdateBo;
import org.micai.platform.resourcesserver.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liucong
 * @since 2021-12-04
 */
public interface UserRoleService extends IService<UserRole> {

    void updateUR(UserRoleUpdateBo bo) throws Exception;

    void saveUR(UserRoleSaveBo bo) throws Exception;

    void delUR(UserRoleDelBo bo) throws Exception;
}
