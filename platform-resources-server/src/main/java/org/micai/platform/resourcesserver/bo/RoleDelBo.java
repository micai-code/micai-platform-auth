package org.micai.platform.resourcesserver.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * @Author liuCong
 * @Date 2021/12/29 16:06
 * @ClassName RoleDelBo
 * @Description
 */
@Data
@ApiModel(value = "删除角色list的bo")
public class RoleDelBo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "idList", required = true)
    @NotEmpty(message = "id不能为空")
    private List<String> idList;
}
