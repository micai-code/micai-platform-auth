package org.micai.platform.resourcesserver.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author liuCong
 * @Date 2021/12/29 15:28
 * @ClassName RoleSaveBo
 * @Description
 */
@Data
@ApiModel(value = "角色保存bo")
public class RoleSaveBo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "角色名称")
    private String roleName;

    @ApiModelProperty(value = "描述")
    private String description;

}
