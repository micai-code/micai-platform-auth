package org.micai.platform.resourcesserver.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.micai.platform.common.base.constant.ConstantCode;
import org.micai.platform.common.base.constant.ConstantEnum;
import org.micai.platform.common.base.exception.PlatformException;
import org.micai.platform.resourcesserver.bo.UserDelBo;
import org.micai.platform.resourcesserver.bo.UserFindBo;
import org.micai.platform.resourcesserver.bo.UserSaveBo;
import org.micai.platform.resourcesserver.bo.UserUpdateBo;
import org.micai.platform.resourcesserver.dto.UserAuthenticationDto;
import org.micai.platform.resourcesserver.entity.User;
import org.micai.platform.resourcesserver.mapper.UserMapper;
import org.micai.platform.resourcesserver.service.UserService;
import org.micai.platform.resourcesserver.utils.AuthenticationManger;
import org.micai.platform.resourcesserver.vo.UserListVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @ClassName UserServiceImpl
 * @Description 用户Service
 * @Author zhaoxinguo
 * @Date 2021/12/3 20:40
 * @Version 1.0
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    protected BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public User findByUsername(String username) {
        if (ObjectUtil.isNotEmpty(username)){
            User user = userMapper.selectOne(new QueryWrapper<User>().lambda().eq(User::getUsername, username));
            return user;
        }
        return null;
    }

    @Override
    public List<User> findUserList() {
        List<User> userList = userMapper.selectList(Wrappers.<User>lambdaQuery());
        return userList;
    }

    @Override
    public Page<UserListVo> getUserList(UserFindBo bo) throws Exception {
        Page<User> userPage = new Page<>(bo.getPageNumber(), bo.getPageSize());
        Page<User> userPageResult = userMapper.selectPage(userPage, new QueryWrapper<User>().lambda()
                .like(ObjectUtil.isNotEmpty(bo.getUsername()),User::getUsername, bo.getUsername())
                .like(ObjectUtil.isNotEmpty(bo.getPhone()),User::getPhone, bo.getPhone())
                .eq(ObjectUtil.isNotEmpty(bo.getStatus()),User::getStatus, bo.getStatus()));
        Page<UserListVo> userListVoPage = new Page<>();
        userListVoPage.setRecords(BeanUtil.copyToList(userPageResult.getRecords(),UserListVo.class));
        return userListVoPage;
    }

    @Override
    public void updateUser(UserUpdateBo bo) throws Exception {
        User user = BeanUtil.copyProperties(bo, User.class);
        UserAuthenticationDto authentication = AuthenticationManger.getAuthentication();
        user.setUpdateUserId(authentication.getId());
        User userEn = userMapper.selectById(user.getId());
        user.setVersion(userEn.getVersion());
        int i = userMapper.updateById(user);
        if (ConstantCode.INT_ONE != i){
            throw new PlatformException(ConstantEnum.FAIL);
        }
    }

    @Override
    public String saveUser(UserSaveBo bo) throws Exception {
        User userOne = userMapper.selectOne(new QueryWrapper<User>().lambda().eq(User::getUsername, bo.getUsername()));
        if (ObjectUtil.isNotEmpty(userOne)) {
            throw new PlatformException(ConstantEnum.USER_EXIST);
        }
        User user = BeanUtil.copyProperties(bo, User.class);
        UserAuthenticationDto authentication = AuthenticationManger.getAuthentication();
        Long id = authentication.getId();
        //生成7位随机密码
        String password = RandomUtil.randomString(ConstantCode.INT_SEVEN);
        String encodePass = bCryptPasswordEncoder.encode(password);
        user.setPassword(encodePass)
                .setCreateUserId(id)
                .setUpdateUserId(id)
                .setStatus(ConstantCode.STR_Z_ONE);
        int i = userMapper.insert(user);
        if (ConstantCode.INT_ONE != i){
            throw new PlatformException(ConstantEnum.UPDATE_ERROR);
        }
        return password;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delUser(UserDelBo bo) throws Exception {
        List<String> idList = bo.getIdList();
        int deleteBatchIds = userMapper.deleteBatchIds(idList);
        if (idList.size() != deleteBatchIds){
            throw new PlatformException(ConstantEnum.UPDATE_ERROR);
        }
    }
}
