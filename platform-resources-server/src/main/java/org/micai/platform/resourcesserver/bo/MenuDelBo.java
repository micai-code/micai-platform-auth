package org.micai.platform.resourcesserver.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @Author liuCong
 * @Date 2021/12/30 9:17
 * @ClassName MenuDelBo
 * @Description
 */
@Data
@ApiModel(value = "菜单删除bo")
public class MenuDelBo {

    @ApiModelProperty(value = "idList", required = true)
    @NotEmpty(message = "id不能为空")
    private List<String> idList;
}
