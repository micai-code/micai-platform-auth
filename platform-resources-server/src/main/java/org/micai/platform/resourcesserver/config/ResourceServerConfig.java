package org.micai.platform.resourcesserver.config;

import org.micai.platform.common.base.config.MicaiPlatformResourcesConfig;
import org.micai.platform.common.base.config.MicaiPlatformTokenConfig;
import org.micai.platform.common.base.constant.ConstantCode;
import org.micai.platform.resourcesserver.filter.AuthHeaderFilter;
import org.micai.platform.resourcesserver.filter.JWTAuthenticationFilter;
import org.micai.platform.resourcesserver.handler.Http401AuthenticationEntryPoint;
import org.micai.platform.resourcesserver.service.PermissionService;
import org.micai.platform.resourcesserver.service.RolePermissionService;
import org.micai.platform.resourcesserver.service.RoleService;
import org.micai.platform.resourcesserver.service.UserRoleService;
import org.micai.platform.resourcesserver.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.security.web.authentication.preauth.x509.X509AuthenticationFilter;

/**
 * @Author liuCong
 * @Date 2022/11/24 下午 5:01
 * @ClassName ResourceServerConfigurerAdapter
 * @Description
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    /**
     * 需要放行的URL
     */
    private static final String[] AUTH_WHITELIST = {
            // -- register url
            "/users/signup",
            "/upload/store",

            // -- swagger ui
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            "/webjars/**",
            "/doc.html",

            // other public endpoints of your API may be appended to this array
    };

    @Autowired
    private JwtTokenStore jwtTokenStore;

    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private RolePermissionService rolePermissionService;

    @Autowired
    private PermissionService permissionService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private MicaiPlatformResourcesConfig micaiPlatformResourcesConfig;

    @Autowired
    private MicaiPlatformTokenConfig micaiPlatformTokenConfig;

    /**
     * 配置:设置oauth2验证规则
     *
     * @param resources 资源
     * @throws Exception 异常
     */
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources
                .resourceId(micaiPlatformResourcesConfig.getResourceIds())//设置资源id
                .tokenStore(jwtTokenStore)
        ;
    }


    /**
     * 设置 HTTP 验证规则
     *
     * @param http
     * @throws Exception
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
            .cors().and().csrf().disable()//禁止跨域
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)//session管理机制: 不会保存session状态

            .and()
            .exceptionHandling()// 自定义异常处理
            .authenticationEntryPoint(new Http401AuthenticationEntryPoint(ConstantCode.BASE_REALM)) // 未登录处理

            .and()
            // 自定义过滤器
            //.addFilter(new JWTAuthenticationFilter(authenticationManager, signKey))
            .authorizeRequests()// 验证策略
            .antMatchers(AUTH_WHITELIST).permitAll()// 无需验证路径
            .anyRequest().authenticated() // 所有请求需要身份认证

            .and()
            .logout() // 默认注销行为为logout，可以通过下面的方式来修改
            .logoutUrl("/logout")
            .logoutSuccessUrl("/")// 设置注销成功后跳转页面，默认是跳转到登录页面;
            .permitAll();

        AuthHeaderFilter authHeaderFilter = new AuthHeaderFilter();
        authHeaderFilter.setAuthHeaderRequestMatcher(micaiPlatformResourcesConfig.getRequestMatcher());
        //为什么要在after
        //因为OAuth2AuthenticationProcessingFilter.doFilter()包含了SecurityContextHolder.clearContext();
        http.addFilterAfter(new JWTAuthenticationFilter(authenticationManager,micaiPlatformTokenConfig), AbstractPreAuthenticatedProcessingFilter.class);
        http.addFilterAfter(authHeaderFilter, X509AuthenticationFilter.class);
    }

}
