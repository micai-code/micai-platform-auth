package org.micai.platform.resourcesserver.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author liuCong
 * @Date 2021/12/30 9:16
 * @ClassName MenuSaveBo
 * @Description
 */
@Data
@ApiModel(value = "菜单保存bo")
public class MenuSaveBo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "菜单名称")
    private String menuName;

    @ApiModelProperty(value = "业务系统标识，区分不同业务系统菜单")
    private String systemCode;

    @ApiModelProperty(value = "业务系统名称")
    private String systemName;

    @ApiModelProperty(value = "终端类型，00：平台")
    private String application;

    @ApiModelProperty(value = "父级菜单编码")
    private String parentCode;

    @ApiModelProperty(value = "菜单图标")
    private String menuIcon;

    @ApiModelProperty(value = "菜单访问路径")
    private String url;

    @ApiModelProperty(value = "菜单打开类型，1:路由 2:新标签页 3:iframe")
    private String openType;

    @ApiModelProperty(value = "序号")
    private Integer sn;


}
