package org.micai.platform.resourcesserver.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.micai.platform.common.base.constant.ConstantEnum;
import org.micai.platform.common.base.exception.PlatformException;
import org.micai.platform.common.base.result.Result;
import org.micai.platform.resourcesserver.bo.RolePermissionDelBo;
import org.micai.platform.resourcesserver.bo.RolePermissionSaveBo;
import org.micai.platform.resourcesserver.bo.RolePermissionUpdateBo;
import org.micai.platform.resourcesserver.service.RolePermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author liucong
 * @since 2022-01-13
 */
@RestController
@RequestMapping("/rolePermission")
@Slf4j
@Api(tags = "角色权限管理", value = "角色权限管理")
public class RolePermissionController extends BaseController{

    @Autowired
    private RolePermissionService rolePermissionService;


    @ApiOperation(value = "更新角色权限", notes = "更新角色权限")
    @PostMapping("/update")
    @PreAuthorize("hasAnyAuthority('sys:rolePermission:update')")
    public Result updateRP(@RequestBody @Valid RolePermissionUpdateBo bo){
        Result result;
        try {
            rolePermissionService.updateRP(bo);
            result = new Result(ConstantEnum.SUCCESS);
        } catch (PlatformException e) {
            log.error("error info:",e);
            result =  new Result(e.getErrorCode(), e.getErrorMessage());
        } catch (Exception e) {
            log.error("error info:",e);
            result =  new Result(ConstantEnum.FAIL);
        }
        return result;
    }

    @ApiOperation(value = "新增角色权限", notes = "新增角色权限")
    @PostMapping("/save")
    @PreAuthorize("hasAnyAuthority('sys:rolePermission:save')")
    public Result saveRP(@RequestBody @Valid RolePermissionSaveBo bo){
        Result result;
        try {
            rolePermissionService.saveRP(bo);
            result = new Result(ConstantEnum.SUCCESS);
        } catch (PlatformException e) {
            log.error("error info:",e);
            result =  new Result(e.getErrorCode(), e.getErrorMessage());
        } catch (Exception e) {
            log.error("error info:",e);
            result =  new Result(ConstantEnum.FAIL);
        }
        return result;
    }

    @ApiOperation(value = "删除角色权限", notes = "删除角色权限")
    @DeleteMapping("/del")
    @PreAuthorize("hasAnyAuthority('sys:rolePermission:del')")
    public Result delRP(@RequestBody @Valid RolePermissionDelBo bo){
        Result result;
        try {
            rolePermissionService.delRP(bo);
            result = new Result(ConstantEnum.SUCCESS);
        } catch (PlatformException e) {
            log.error("error info:",e);
            result =  new Result(e.getErrorCode(), e.getErrorMessage());
        } catch (Exception e) {
            log.error("error info:",e);
            result =  new Result(ConstantEnum.FAIL);
        }
        return result;
    }
}

