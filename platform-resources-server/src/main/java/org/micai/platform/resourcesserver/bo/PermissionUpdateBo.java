package org.micai.platform.resourcesserver.bo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author liuCong
 * @Date 2021/12/29 16:50
 * @ClassName PermissionUpdateBo
 * @Description
 */
@Data
@ApiModel(value = "权限更新bo")
public class PermissionUpdateBo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "权限编码")
    private String permissionCode;

    @ApiModelProperty(value = "权限名称")
    private String permissionName;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "状态: 01 启用 02停用")
    private String status;


}
