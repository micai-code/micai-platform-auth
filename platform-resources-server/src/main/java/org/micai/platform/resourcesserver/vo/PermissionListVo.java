package org.micai.platform.resourcesserver.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author liuCong
 * @Date 2021/12/29 17:22
 * @ClassName PermissionListVo
 * @Description
 */
@Data
@ApiModel(value = "权限列表")
public class PermissionListVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    private Long id;

    @ApiModelProperty(value = "权限编码")
    private String permissionCode;

    @ApiModelProperty(value = "权限名称")
    private String permissionName;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "状态: 01 启用 02停用")
    private String status;

    @ApiModelProperty(value = "创建人")
    private Long createUserId;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "修改人")
    private Long updateUserId;

    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

}
