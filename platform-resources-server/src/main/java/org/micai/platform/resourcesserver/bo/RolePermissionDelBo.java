package org.micai.platform.resourcesserver.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * @Author liuCong
 * @Date 2022/1/13 15:11
 * @ClassName RolePermissionDelBo
 * @Description
 */
@Data
@ApiModel(value = "用户删除")
public class RolePermissionDelBo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "idList", required = true)
    @NotEmpty(message = "id不能为空")
    private List<String> idList;
}
