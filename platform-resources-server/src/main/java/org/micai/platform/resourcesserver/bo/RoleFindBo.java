package org.micai.platform.resourcesserver.bo;

import cn.hutool.db.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author liuCong
 * @Date 2021/12/29 15:35
 * @ClassName RoleFindBo
 * @Description
 */
@Data
@ApiModel(value = "查询角色bo")
public class RoleFindBo extends Page implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "角色编码")
    private String roleCode;

    @ApiModelProperty(value = "角色名称")
    private String roleName;

    @ApiModelProperty(value = "状态: 01 启用 02停用")
    private String status;

}
