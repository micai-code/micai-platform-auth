package org.micai.platform.resourcesserver.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author liuCong
 * @Date 2021/12/29 16:47
 * @ClassName PermissionSaveBo
 * @Description
 */
@Data
@ApiModel(value = "权限保存bo")
public class PermissionSaveBo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "权限编码")
    private String permissionCode;

    @ApiModelProperty(value = "权限名称")
    private String permissionName;

    @ApiModelProperty(value = "描述")
    private String description;

}
