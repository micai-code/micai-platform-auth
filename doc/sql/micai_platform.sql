/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50736
 Source Host           : localhost:3306
 Source Schema         : security

 Target Server Type    : MySQL
 Target Server Version : 50736
 File Encoding         : 65001

 Date: 26/12/2022 15:51:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for oauth_client_details
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_details`;
CREATE TABLE `oauth_client_details` (
  `client_id` varchar(255) NOT NULL,
  `resource_ids` varchar(256) DEFAULT NULL,
  `client_secret` varchar(256) DEFAULT NULL,
  `scope` varchar(256) DEFAULT NULL,
  `authorized_grant_types` varchar(256) DEFAULT NULL,
  `web_server_redirect_uri` varchar(256) DEFAULT NULL,
  `authorities` varchar(256) DEFAULT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additional_information` varchar(4096) DEFAULT NULL,
  `autoapprove` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`client_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of oauth_client_details
-- ----------------------------
INSERT INTO `oauth_client_details` VALUES ('pc', 'resources-server', '$2a$10$XyKNPWArxeVGjOKFF.pF7ONmv9NMAN5Bkkiwy8MutFTUKXhHFv.zC', 'pc', 'authorization_code,password,refresh_token', 'http://www.baidu.com/', 'info,sys:user:list', NULL, NULL, NULL, 'true');

-- ----------------------------
-- Table structure for oauth_code
-- ----------------------------
DROP TABLE IF EXISTS `oauth_code`;
CREATE TABLE `oauth_code`  (
  `code` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `authentication` blob NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for tb_organ_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_organ_user`;
CREATE TABLE `tb_organ_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `organ_id` bigint(20) NOT NULL COMMENT '组织机构id',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NULL DEFAULT NULL COMMENT '修改用户id',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `version` bigint(20) NOT NULL DEFAULT 0 COMMENT '版本号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_organ_user
-- ----------------------------

-- ----------------------------
-- Table structure for tb_permission
-- ----------------------------
DROP TABLE IF EXISTS `tb_permission`;
CREATE TABLE `tb_permission`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `permission_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限编码',
  `permission_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限名称',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '01' COMMENT '状态: 01 启用 02停用',
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `version` bigint(20) NULL DEFAULT 0 COMMENT '版本',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_permission
-- ----------------------------
INSERT INTO `tb_permission` VALUES (1, 'sys:permission:save', '新增权限', '新增权限', '01', 1, '2022-01-13 12:58:32', 1, '2022-01-13 12:58:32', 0);
INSERT INTO `tb_permission` VALUES (2, 'sys:user:list', '获取用户列表', '获取用户列表', '01', 1, '2022-01-13 13:41:09', 1, '2022-01-13 13:41:09', 0);
INSERT INTO `tb_permission` VALUES (3, 'sys:user:update', '更新用户', '更新用户', '01', 1, '2022-01-13 13:43:59', 1, '2022-01-13 13:43:59', 0);
INSERT INTO `tb_permission` VALUES (4, 'sys:user:save', '新增用户', '新增用户', '01', 1, '2022-01-13 13:44:28', 1, '2022-01-13 13:44:28', 0);
INSERT INTO `tb_permission` VALUES (5, 'sys:user:del', '删除用户', '删除用户', '01', 1, '2022-01-13 13:44:46', 1, '2022-01-13 13:44:46', 0);
INSERT INTO `tb_permission` VALUES (6, 'sys:sysMenu:list', '查询菜单', '查询菜单', '01', 1, '2022-01-13 13:45:18', 1, '2022-01-13 13:45:18', 0);
INSERT INTO `tb_permission` VALUES (7, 'sys:sysMenu:update', '更新菜单', '更新菜单', '01', 1, '2022-01-13 13:45:50', 1, '2022-01-13 13:45:50', 0);
INSERT INTO `tb_permission` VALUES (8, 'sys:sysMenu:save', '新增菜单', '新增菜单', '01', 1, '2022-01-13 13:46:31', 1, '2022-01-13 13:46:31', 0);
INSERT INTO `tb_permission` VALUES (9, 'sys:sysMenu:del', '删除菜单', '删除菜单', '01', 1, '2022-01-13 13:47:31', 1, '2022-01-13 13:47:31', 0);
INSERT INTO `tb_permission` VALUES (10, 'sys:role:list', '获取角色列表', '获取角色列表', '01', 1, '2022-01-13 13:48:10', 1, '2022-01-13 13:48:10', 0);
INSERT INTO `tb_permission` VALUES (11, 'sys:role:update', '更新角色信息', '更新角色信息', '01', 1, '2022-01-13 13:48:25', 1, '2022-01-13 13:48:25', 0);
INSERT INTO `tb_permission` VALUES (12, 'sys:role:save', '新增角色', '新增角色', '01', 1, '2022-01-13 13:48:47', 1, '2022-01-13 13:48:47', 0);
INSERT INTO `tb_permission` VALUES (13, 'sys:role:del', '删除角色', '删除角色', '01', 1, '2022-01-13 13:49:38', 1, '2022-01-13 13:49:38', 0);
INSERT INTO `tb_permission` VALUES (14, 'sys:permission:list', '获取权限列表', '获取权限列表', '01', 1, '2022-01-13 13:50:36', 1, '2022-01-13 13:50:36', 0);
INSERT INTO `tb_permission` VALUES (15, 'sys:permission:update', '更新权限信息', '更新权限信息', '01', 1, '2022-01-13 13:50:59', 1, '2022-01-13 13:50:59', 0);
INSERT INTO `tb_permission` VALUES (16, 'sys:permission:del', '删除权限', '删除权限', '01', 1, '2022-01-13 13:52:00', 1, '2022-01-13 13:52:00', 0);
INSERT INTO `tb_permission` VALUES (18, 'sys:rolePermission:update', '更新角色', '更新角色', '01', 1, '2022-01-13 15:28:10', 1, '2022-01-13 15:28:10', 0);
INSERT INTO `tb_permission` VALUES (19, 'sys:rolePermission:save', '新增角色', '新增角色', '01', 1, '2022-01-13 15:28:31', 1, '2022-01-13 15:28:31', 0);
INSERT INTO `tb_permission` VALUES (20, 'sys:rolePermission:del', '删除角色', '删除角色', '01', 1, '2022-01-13 15:28:55', 1, '2022-01-13 15:28:55', 0);
INSERT INTO `tb_permission` VALUES (21, 'sys:permissionMenu:update', '更新角色菜单', '更新角色菜单', '01', 1, '2022-01-13 17:34:33', 1, '2022-01-13 17:34:33', 0);
INSERT INTO `tb_permission` VALUES (22, 'sys:permissionMenu:save', '新增角色菜单', '新增角色菜单', '01', 1, '2022-01-13 17:34:51', 1, '2022-01-13 17:34:51', 0);
INSERT INTO `tb_permission` VALUES (23, 'sys:permissionMenu:del', '删除角色菜单', '删除角色菜单', '01', 1, '2022-01-13 17:35:08', 1, '2022-01-13 17:35:08', 0);
INSERT INTO `tb_permission` VALUES (24, 'sys:userRole:update', '更新用户角色', '更新用户角色', '01', 1, '2022-01-14 10:41:35', 1, '2022-01-14 10:41:35', 0);
INSERT INTO `tb_permission` VALUES (25, 'sys:userRole:save', '新增用户角色', '新增用户角色', '01', 1, '2022-01-14 10:41:54', 1, '2022-01-14 10:41:54', 0);
INSERT INTO `tb_permission` VALUES (26, 'sys:userRole:del', '删除用户角色', '删除用户角色', '01', 1, '2022-01-14 10:42:14', 1, '2022-01-14 10:42:14', 0);
INSERT INTO `tb_permission` VALUES (27, 'sys:user:authorityList', '获取用户权限', '获取用户权限', '01', 1, '2022-12-27 11:18:16', 1, '2022-12-27 11:18:24', 0);

-- ----------------------------
-- Table structure for tb_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_role`;
CREATE TABLE `tb_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色编码',
  `role_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '01' COMMENT '状态: 01 启用 02停用',
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `version` bigint(20) NULL DEFAULT 0 COMMENT '版本号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_role
-- ----------------------------
INSERT INTO `tb_role` VALUES (1, 'ROLE_root', '院长', '最高权限', '01', NULL, '2021-12-04 14:12:34', NULL, '2021-12-04 14:12:38', 1);

-- ----------------------------
-- Table structure for tb_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `tb_role_permission`;
CREATE TABLE `tb_role_permission`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NOT NULL COMMENT '角色id',
  `permission_id` bigint(20) NOT NULL COMMENT '权限id',
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NULL DEFAULT NULL COMMENT '修改用户id',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `version` bigint(20) NOT NULL DEFAULT 0 COMMENT '版本号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_role_permission
-- ----------------------------
INSERT INTO `tb_role_permission` VALUES (1, 1, 1, 1, '2022-01-13 16:38:37', 1, '2022-01-13 16:38:37', 0);
INSERT INTO `tb_role_permission` VALUES (2, 1, 19, 1, '2022-01-13 16:38:40', 1, '2022-01-13 16:38:40', 0);
INSERT INTO `tb_role_permission` VALUES (10, 1, 2, 1, '2022-01-13 16:36:09', 1, '2022-01-13 16:36:09', 0);
INSERT INTO `tb_role_permission` VALUES (13, 1, 3, 1, '2022-01-13 16:36:47', 1, '2022-01-13 16:36:47', 0);
INSERT INTO `tb_role_permission` VALUES (14, 1, 4, 1, '2022-01-13 16:36:51', 1, '2022-01-13 16:36:51', 0);
INSERT INTO `tb_role_permission` VALUES (15, 1, 5, 1, '2022-01-13 16:36:54', 1, '2022-01-13 16:36:54', 0);
INSERT INTO `tb_role_permission` VALUES (16, 1, 6, 1, '2022-01-13 16:36:58', 1, '2022-01-13 16:36:58', 0);
INSERT INTO `tb_role_permission` VALUES (17, 1, 7, 1, '2022-01-13 16:37:02', 1, '2022-01-13 16:37:02', 0);
INSERT INTO `tb_role_permission` VALUES (18, 1, 8, 1, '2022-01-13 16:37:07', 1, '2022-01-13 16:37:07', 0);
INSERT INTO `tb_role_permission` VALUES (19, 1, 9, 1, '2022-01-13 16:37:10', 1, '2022-01-13 16:37:10', 0);
INSERT INTO `tb_role_permission` VALUES (20, 1, 10, 1, '2022-01-13 16:37:14', 1, '2022-01-13 16:37:14', 0);
INSERT INTO `tb_role_permission` VALUES (21, 1, 11, 1, '2022-01-13 16:37:17', 1, '2022-01-13 16:37:17', 0);
INSERT INTO `tb_role_permission` VALUES (22, 1, 12, 1, '2022-01-13 16:37:20', 1, '2022-01-13 16:37:20', 0);
INSERT INTO `tb_role_permission` VALUES (23, 1, 13, 1, '2022-01-13 16:37:23', 1, '2022-01-13 16:37:23', 0);
INSERT INTO `tb_role_permission` VALUES (24, 1, 14, 1, '2022-01-13 16:37:27', 1, '2022-01-13 16:37:27', 0);
INSERT INTO `tb_role_permission` VALUES (25, 1, 15, 1, '2022-01-13 16:37:32', 1, '2022-01-13 16:37:32', 0);
INSERT INTO `tb_role_permission` VALUES (26, 1, 16, 1, '2022-01-13 16:37:45', 1, '2022-01-13 16:37:45', 0);
INSERT INTO `tb_role_permission` VALUES (27, 1, 17, 1, '2022-01-13 16:37:49', 1, '2022-01-13 16:37:49', 0);
INSERT INTO `tb_role_permission` VALUES (28, 1, 18, 1, '2022-01-13 16:37:53', 1, '2022-01-13 16:37:53', 0);
INSERT INTO `tb_role_permission` VALUES (29, 1, 20, 1, '2022-01-13 16:38:20', 1, '2022-01-13 16:38:20', 0);
INSERT INTO `tb_role_permission` VALUES (30, 1, 21, 1, '2022-01-13 17:36:25', 1, '2022-01-13 17:36:25', 0);
INSERT INTO `tb_role_permission` VALUES (31, 1, 22, 1, '2022-01-13 17:36:25', 1, '2022-01-13 17:36:25', 0);
INSERT INTO `tb_role_permission` VALUES (32, 1, 23, 1, '2022-01-13 17:36:25', 1, '2022-01-13 17:36:25', 0);
INSERT INTO `tb_role_permission` VALUES (33, 1, 27, 1, '2022-12-27 11:19:38', 1, '2022-12-27 11:19:46', 0);

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '密码',
  `phone` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '手机号',
  `email` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '邮箱',
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `status` char(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '01' COMMENT '状态: 01 启用 02停用',
  `version` bigint(20) NULL DEFAULT 0 COMMENT '版本',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES (1, 'root', '$2a$10$WJRzfWrjox1WLF//ZSIug.oYUTqGRUFfNxCeCvBce3YJarZqodmua', NULL, NULL, NULL, NULL, NULL, NULL, '01', 0);

-- ----------------------------
-- Table structure for tb_user_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_role`;
CREATE TABLE `tb_user_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `role_id` bigint(20) NOT NULL COMMENT '角色id',
  `creat_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建用户id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NULL DEFAULT NULL COMMENT '修改用户id',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `version` bigint(20) NOT NULL DEFAULT 0 COMMENT '版本号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user_role
-- ----------------------------
INSERT INTO `tb_user_role` VALUES (1, 1, 1, 1, '2021-12-04 15:46:04', NULL, NULL, 0);

SET FOREIGN_KEY_CHECKS = 1;
