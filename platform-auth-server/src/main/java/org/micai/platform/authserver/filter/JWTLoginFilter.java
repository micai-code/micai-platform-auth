package org.micai.platform.authserver.filter;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.micai.platform.authserver.bo.UserQueryBo;
import org.micai.platform.authserver.handler.CustomAuthenticationFailureHandler;
import org.micai.platform.common.base.config.MicaiPlatformTokenConfig;
import org.micai.platform.common.base.constant.ConstantCode;
import org.micai.platform.common.base.constant.ConstantEnum;
import org.micai.platform.common.base.result.Result;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 自定义JWT登录过滤器
 * 验证用户名密码正确后，生成一个token，并将token返回给客户端
 * 该类继承自UsernamePasswordAuthenticationFilter，重写了其中的2个方法
 * attemptAuthentication ：接收并解析用户凭证。
 * successfulAuthentication ：用户成功登录后，这个方法会被调用，我们在这个方法里生成token。
 *
 * @author zhaoxinguo on 2017/9/12.
 */
@Slf4j
public class JWTLoginFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;

    private final MicaiPlatformTokenConfig micaiPlatformTokenConfig;

    private final CustomAuthenticationFailureHandler customAuthenticationFailureHandler;

    public JWTLoginFilter(AuthenticationManager authenticationManager, MicaiPlatformTokenConfig micaiPlatformTokenConfig, CustomAuthenticationFailureHandler customAuthenticationFailureHandler) {
        this.authenticationManager = authenticationManager;
        this.micaiPlatformTokenConfig = micaiPlatformTokenConfig;
        this.customAuthenticationFailureHandler = customAuthenticationFailureHandler;
    }

    /**
     * 尝试身份认证(接收并解析用户凭证)
     *
     * @param req
     * @param res
     * @return
     * @throws AuthenticationException
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) {
        Authentication authenticate = null;
        try {
            Map<String, String> requestMap = new ObjectMapper().readValue(req.getInputStream(), HashMap.class);
            UserQueryBo userQueryBo = new UserQueryBo()
                    .setUsername(requestMap.get(ConstantCode.USERNAME))
                    .setPassword(requestMap.get(ConstantCode.PASSWORD));
            authenticate = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            userQueryBo.getUsername(),
                            userQueryBo.getPassword(),
                            new ArrayList<>())
            );
        } catch (AuthenticationException e) {
            try {
                customAuthenticationFailureHandler.onAuthenticationFailure(req, res, e);
            } catch (IOException ex) {
                log.error("IOException:", ex);
            } catch (ServletException ex) {
                log.error("ServletException:", ex);
            }
        } catch (Exception e) {
            log.error("Exception:", e);
            try {
                // 捕获权限异常返回到前端的body里面
                res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                res.setContentType(ConstantCode.CONTENT_TYPE_JSON);
                Result result = new Result(ConstantEnum.FAIL);
                PrintWriter out = res.getWriter();
                out.write(new ObjectMapper().writeValueAsString(result));
                out.flush();
                out.close();
            } catch (IOException ee) {
                log.error("IOException:", ee);
            }
        }
        return authenticate;
    }

    /**
     * 认证成功(用户成功登录后，这个方法会被调用，我们在这个方法里生成token)
     *
     * @param request
     * @param response
     * @param chain
     * @param auth
     * @throws IOException
     * @throws ServletException
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {
        // builder the token
        String token = null;
        try {
            Collection<? extends GrantedAuthority> authorities = auth.getAuthorities();
            // 定义存放角色集合的对象
            List<String> roleList = new ArrayList<>();
            for (GrantedAuthority grantedAuthority : authorities) {
                roleList.add(grantedAuthority.getAuthority());
            }

            // 生成token start
            Calendar calendar = Calendar.getInstance();
            Date now = calendar.getTime();
            // 设置签发时间
            calendar.setTime(new Date());
            // 设置过期时间
            calendar.add(Calendar.SECOND, micaiPlatformTokenConfig.getTimeout());
            Date time = calendar.getTime();
            String rolesStr = JSON.toJSONString(roleList);
            token = Jwts.builder()
                    .setSubject(auth.getName() + "-" + rolesStr)
                    .setIssuedAt(now)//签发时间
                    .setExpiration(time)//过期时间
                    .signWith(SignatureAlgorithm.HS512, micaiPlatformTokenConfig.getSignKey()) //采用什么算法是可以自己选择的，不一定非要采用HS512
                    .compact();
            // 生成token end
            // 登录成功后，返回token到body里面
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put(micaiPlatformTokenConfig.getTokenName(), micaiPlatformTokenConfig.getTokenPrefix() + " " + token);
            Result result = new Result(ConstantEnum.SUCCESS, resultMap);
            response.setContentType(ConstantCode.CONTENT_TYPE_JSON);
            response.setStatus(HttpServletResponse.SC_OK);
            PrintWriter writer = response.getWriter();
            writer.print(JSON.toJSONString(result));
            writer.flush();
            writer.close();
        } catch (Exception e) {
            log.error("successfulAuthentication error:{}", e.toString());
        }
    }

}
