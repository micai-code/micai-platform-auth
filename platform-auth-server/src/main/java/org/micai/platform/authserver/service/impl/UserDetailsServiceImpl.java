package org.micai.platform.authserver.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.micai.platform.authserver.entity.*;
import org.micai.platform.authserver.service.*;
import org.micai.platform.common.base.constant.ConstantCode;
import org.micai.platform.common.base.constant.ConstantEnum;
import org.micai.platform.common.base.exception.MyAuthException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author liuCong
 * @Date 2022/11/24 下午 1:36
 * @ClassName UserDetailsServiceImpl
 * @Description
 */
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private RolePermissionService rolePermissionService;

    @Autowired
    private PermissionService permissionService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.getOne(new LambdaQueryWrapper<User>().eq(User::getUsername,username));
        if (user == null){
            throw new UsernameNotFoundException("未找到该用户");
        }
        //获取用户权限
        List<UserRole> userRoleList = userRoleService.list(new QueryWrapper<UserRole>().lambda().eq(UserRole::getUserId,user.getId()));
        if (ObjectUtil.isEmpty(userRoleList)){
            throw new MyAuthException(ConstantEnum.NO_ROLE);
        }
        List<Long> roleIdList = userRoleList.stream().map(UserRole::getRoleId).collect(Collectors.toList());
        List<Role> roleList = roleService.list(new QueryWrapper<Role>().lambda()
                .in(Role::getId,roleIdList).eq(Role::getStatus, ConstantCode.STR_Z_ONE));
        if (ObjectUtil.isEmpty(roleList)){
            throw new MyAuthException(ConstantEnum.NO_ROLE);
        }
        List<RolePermission> rolePermissionList = rolePermissionService.list(new QueryWrapper<RolePermission>().lambda()
                .in(RolePermission::getRoleId,roleIdList));
        if (ObjectUtil.isEmpty(rolePermissionList)){
            throw new MyAuthException(ConstantEnum.NO_PERMISSION);
        }
        List<Long> permissionIdList = rolePermissionList.stream().map(RolePermission::getPermissionId).collect(Collectors.toList());
        List<Permission> permissionList = permissionService.list(new QueryWrapper<Permission>().lambda()
                .in(Permission::getId,permissionIdList).eq(Permission::getStatus,ConstantCode.STR_Z_ONE));
        if (ObjectUtil.isEmpty(permissionList)){
            throw new MyAuthException(ConstantEnum.NO_PERMISSION);
        }

        String collect = permissionList.stream().map(Permission::getPermissionCode).collect(Collectors.joining(","));
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), AuthorityUtils.createAuthorityList(collect));
    }

}
