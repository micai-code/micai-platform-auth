package org.micai.platform.authserver.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.micai.platform.authserver.entity.Permission;
import org.micai.platform.authserver.mapper.PermissionMapper;
import org.micai.platform.authserver.service.PermissionService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liucong
 * @since 2021-12-04
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

}
