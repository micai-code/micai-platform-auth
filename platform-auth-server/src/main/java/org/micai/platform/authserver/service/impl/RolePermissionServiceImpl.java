package org.micai.platform.authserver.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.micai.platform.authserver.entity.RolePermission;
import org.micai.platform.authserver.mapper.RolePermissionMapper;
import org.micai.platform.authserver.service.RolePermissionService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liucong
 * @since 2021-12-04
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

}
