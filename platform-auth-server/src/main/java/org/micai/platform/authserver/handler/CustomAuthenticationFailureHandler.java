package org.micai.platform.authserver.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.micai.platform.common.base.constant.ConstantCode;
import org.micai.platform.common.base.result.Result;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @Author liuCong
 * @Date 2022/1/4 17:20
 * @ClassName CustomAuthenticationFailureHandler
 * @Description
 */
@Component
public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType(ConstantCode.CONTENT_TYPE_JSON);
        Result result = new Result(HttpServletResponse.SC_UNAUTHORIZED,exception.getMessage());
        PrintWriter out = response.getWriter();
        out.write(new ObjectMapper().writeValueAsString(result));
        out.flush();
        out.close();
    }
}
