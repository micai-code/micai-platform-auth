package org.micai.platform.authserver.config;

import org.micai.platform.authserver.filter.JWTAuthenticationFilter;
import org.micai.platform.authserver.filter.JWTLoginFilter;
import org.micai.platform.authserver.handler.CustomAuthenticationFailureHandler;
import org.micai.platform.authserver.handler.Http401AuthenticationEntryPoint;
import org.micai.platform.authserver.provider.CustomAuthenticationProvider;
import org.micai.platform.authserver.service.PermissionService;
import org.micai.platform.authserver.service.RolePermissionService;
import org.micai.platform.authserver.service.RoleService;
import org.micai.platform.authserver.service.UserRoleService;
import org.micai.platform.authserver.service.UserService;
import org.micai.platform.authserver.service.impl.UserDetailsServiceImpl;
import org.micai.platform.common.base.config.MicaiPlatformTokenConfig;
import org.micai.platform.common.base.constant.ConstantCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * SpringSecurity的配置
 * 通过SpringSecurity的配置，将JWTLoginFilter，JWTAuthenticationFilter组合在一起
 *
 * @author zhaoxinguo on 2017/9/13.
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private RolePermissionService rolePermissionService;

    @Autowired
    private PermissionService permissionService;

    @Autowired
    private CustomAuthenticationFailureHandler customAuthenticationFailureHandler;

    @Autowired
    private MicaiPlatformTokenConfig micaiPlatformTokenConfig;

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    /**
     * 设置 HTTP 验证规则
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .cors().and().csrf().disable()//禁止跨域
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)//session管理机制: 不会保存session状态

            .and()
            .exceptionHandling()// 自定义异常处理
            .authenticationEntryPoint(new Http401AuthenticationEntryPoint(ConstantCode.BASE_REALM)) // 未登录处理
//                .and()//权限不足处理.exceptionHandling().accessDeniedHandler(customAccessDeniedHandler) // 自定义访问失败处理器

            .and()
            // 自定义过滤器
            .addFilter(new JWTLoginFilter(authenticationManager(), micaiPlatformTokenConfig, customAuthenticationFailureHandler))
            .addFilter(new JWTAuthenticationFilter(authenticationManager(), micaiPlatformTokenConfig))
            .authorizeRequests()// 验证策略
            .anyRequest().authenticated() // 所有请求需要身份认证

            .and()
            .logout() // 默认注销行为为logout，可以通过下面的方式来修改
            .logoutUrl("/logout")
            .logoutSuccessUrl("/login")// 设置注销成功后跳转页面，默认是跳转到登录页面;
//                .logoutSuccessHandler(customLogoutSuccessHandler)
            .permitAll();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return new UserDetailsServiceImpl();
    }

    /**
     * 该方法是登录的时候会进入
     *
     * @param auth
     * @throws Exception
     */
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        // 使用自定义身份验证组件
        auth.authenticationProvider(new CustomAuthenticationProvider(userService, bCryptPasswordEncoder,userRoleService,
                roleService,rolePermissionService,permissionService));
    }
}
