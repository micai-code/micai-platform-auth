package org.micai.platform.authserver.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Author liuCong
 * @Date 2021/12/11 11:36
 * @ClassName MyMetaObjectHandler
 * @Description
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    private final String CREATE_TIME = "createTime";
    private final String UPDATE_TIME = "updateTime";
    private final String VERSION = "version";
    /**
     * 插入时的填充策略
     * @param metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        this.setFieldValByName(CREATE_TIME, new Date(), metaObject);
        this.setFieldValByName(UPDATE_TIME, new Date(), metaObject);
    }

    /**
     * 更新时的填充策略
     * @param metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName(UPDATE_TIME, new Date(), metaObject);
    }
}
