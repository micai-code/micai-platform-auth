package org.micai.platform.authserver.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.micai.platform.authserver.entity.Role;
import org.micai.platform.authserver.mapper.RoleMapper;
import org.micai.platform.authserver.service.RoleService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liucong
 * @since 2021-12-04
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

}
