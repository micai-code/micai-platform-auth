package org.micai.platform.authserver.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.micai.platform.authserver.entity.User;

public interface UserService extends IService<User> {

}
