package org.micai.platform.authserver.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.micai.platform.authserver.entity.UserRole;
import org.micai.platform.authserver.mapper.UserRoleMapper;
import org.micai.platform.authserver.service.UserRoleService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liucong
 * @since 2021-12-04
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {


}
