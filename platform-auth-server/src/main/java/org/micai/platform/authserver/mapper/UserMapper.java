package org.micai.platform.authserver.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.micai.platform.authserver.entity.User;

public interface UserMapper extends BaseMapper<User> {

}
