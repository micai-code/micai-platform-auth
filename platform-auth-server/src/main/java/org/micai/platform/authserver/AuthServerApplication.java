package org.micai.platform.authserver;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author liuCong
 * @Date ${DATE} ${TIME}
 * @ClassName ${NAME}
 * @Description 认证服务的启动类
 */
@SpringBootApplication
@MapperScan("org.micai.platform.authserver.mapper")
public class AuthServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthServerApplication.class,args);
    }
}