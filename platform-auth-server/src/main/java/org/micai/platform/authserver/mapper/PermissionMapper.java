package org.micai.platform.authserver.mapper;

import org.micai.platform.authserver.entity.Permission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liucong
 * @since 2021-12-04
 */
public interface PermissionMapper extends BaseMapper<Permission> {

}
